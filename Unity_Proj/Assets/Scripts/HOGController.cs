using UnityEngine;
using System.Collections;

//------------------------------------------------------------------------------
// class definition
//------------------------------------------------------------------------------
public class HOGController : MonoBehaviour
{
	static private HOGController hogController;
	
	private ItemController itemController;
	private Rect screenRect;

	//----------------------------------------------------------------------
	// static public methods
	//----------------------------------------------------------------------
	//----------------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------------
	//----------------------------------------------------------------------
	// protected mono methods
	//----------------------------------------------------------------------
	protected void Awake()
	{
		hogController = this;
	}
	
	protected void OnDestroy()
	{
		if(hogController != null)
		{
			hogController = null;
		}
	}
	
	protected void OnDisable()
	{
	}
	
	protected void OnEnable()
	{
	}
	
	protected void OnGUI()
	{
		GUILayout.BeginArea(screenRect);
		GUILayout.BeginVertical();
		GUILayout.Label(Input.mousePosition.ToString());
		if(itemController != null)
		{
			GUILayout.Label(itemController.name);
		}
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
	
	protected void Start()
	{
		screenRect = new Rect(0, 0, Screen.width, Screen.height);
		Camera.main.orthographicSize = (Screen.height/2.0f);
	}
	
	protected void Update()
	{
		itemController = ItemController.PickItem(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		if((Input.GetMouseButtonDown(0) == true) && (itemController.IsItem() == true))
		{
			itemController.PickUpItem();
		}

		if(Input.GetKeyDown(KeyCode.D) == true)
		{
			ItemController.WriteItemListToConsole();
		}
	}

	//----------------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------------
}
