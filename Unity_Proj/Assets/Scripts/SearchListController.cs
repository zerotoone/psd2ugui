//#define LOG_TRACE_INFO
//#define LOG_EXTRA_INFO

using UnityEngine;
using System.Collections;

//------------------------------------------------------------------------------
// class definition
//------------------------------------------------------------------------------
public class SearchListController : MonoBehaviour
{
	private const int MAX_ITEMS_IN_LEFTRIGHT_LIST = 9;
	private const int MAX_ITEMS_IN_TOPBOTTOM_LIST = 6;
	private const int MAX_ITEMS_TO_FIND = 9;
	
	private float SEARCH_ITEM_HEIGHT = 28;
	private float SEARCH_ITEM_WIDTH = 124;
	private float SEARCH_ITEM_PADDING = 8;

	static private SearchListController searchListController;
	
	[SerializeField]
	private int totalItemCount;

	private Rect searchItemBounds;
	private enum SearchListAnchor { Invisible, Bottom, Left, Right, Top };
	private SearchListAnchor searchListAnchor;
	private Rect searchListBounds;
	private int searchListItemCount;
	
	private ItemController[] searchItemControllers;
	private delegate void UpdateDelegate();
	private UpdateDelegate onGuiUpdateDelegate;
	private UpdateDelegate updateDelegate;

	//----------------------------------------------------------------------
	// static public methods
	//----------------------------------------------------------------------
	//----------------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------------
	public void SetTotalItemCount(int totalItemCount)
	{
		this.totalItemCount = totalItemCount;
	}
	
	//----------------------------------------------------------------------
	// protected mono methods
	//----------------------------------------------------------------------
	protected void Awake()
	{
		searchListController = this;
	}
	
	protected void OnDestroy()
	{
		if(searchListController != null)
		{
			searchListController = null;
		}
	}
	
	protected void OnDisable()
	{
	}
	
	protected void OnEnable()
	{
	}
	
	protected void OnGUI()
	{
		if(onGuiUpdateDelegate != null)
		{
			onGuiUpdateDelegate();
		}
	}
	
	protected void Start()
	{
		// set default location for search list
		searchListAnchor = SearchListAnchor.Bottom;
		switch(searchListAnchor)
		{
			case SearchListAnchor.Invisible:
				searchListBounds = new Rect();
				break;
			case SearchListAnchor.Left:
				searchListBounds = new Rect(0, 0, SEARCH_ITEM_WIDTH, Camera.main.pixelHeight);
				searchListItemCount = MAX_ITEMS_IN_LEFTRIGHT_LIST;
				break;
			case SearchListAnchor.Right:
				searchListBounds = new Rect(Camera.main.pixelWidth - SEARCH_ITEM_WIDTH, 0, SEARCH_ITEM_WIDTH, Camera.main.pixelHeight);
				searchListItemCount = MAX_ITEMS_IN_LEFTRIGHT_LIST;
				break;
			case SearchListAnchor.Top:
				searchListBounds = new Rect(0, 0, Camera.main.pixelWidth, SEARCH_ITEM_HEIGHT * 3);
				searchListItemCount = MAX_ITEMS_IN_TOPBOTTOM_LIST;
				break;
			case SearchListAnchor.Bottom:
				searchListBounds = new Rect(0, Camera.main.pixelHeight - (SEARCH_ITEM_HEIGHT * 3), Camera.main.pixelWidth, SEARCH_ITEM_HEIGHT * 3);
				searchListItemCount = MAX_ITEMS_IN_TOPBOTTOM_LIST;
				break;
		}
		searchItemBounds = new Rect();
		updateDelegate = UpdateSearchListInitialization;
	}
	
	protected void Update()
	{
		if(updateDelegate != null)
		{
			updateDelegate();
		}
	}

	//----------------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------------
	private void OnGuiDrawSearchList()
	{
		// using search list?
		if(searchListAnchor == SearchListAnchor.Invisible)
		{
			return;
		}
		
		// show the search list
		// make a scroll area
		GUI.Box(searchListBounds, "");
		GUI.BeginScrollView(searchListBounds, Vector2.zero, searchListBounds);
		// loop and add items
		int itemNumber = 0;
		searchItemBounds.x = searchListBounds.x + SEARCH_ITEM_PADDING;
		searchItemBounds.y = searchListBounds.y + SEARCH_ITEM_PADDING;
		searchItemBounds.width = SEARCH_ITEM_WIDTH;
		searchItemBounds.height = SEARCH_ITEM_HEIGHT;
		for(int i = 0; (i < searchItemControllers.Length) && (i < searchListItemCount); i++)
		{
			if(searchItemControllers[i].gameObject.activeSelf == false)
			{
				continue;
			}
			GUI.Label(searchItemBounds, searchItemControllers[i].name);
			switch(searchListAnchor)
			{
				case SearchListAnchor.Left:
				case SearchListAnchor.Right:
					searchItemBounds.y += searchItemBounds.height;
					break;
				case SearchListAnchor.Top:
				case SearchListAnchor.Bottom:
					searchItemBounds.x += searchItemBounds.width;
					if(searchItemBounds.x > searchListBounds.width - searchItemBounds.width)
					{
						searchItemBounds.x = searchListBounds.x + SEARCH_ITEM_PADDING;
						searchItemBounds.y += searchItemBounds.height;
					}
					break;
			}
			itemNumber++;
		}
		GUI.EndScrollView();
	}
	
	private void UpdateSearchListInitialization()
	{
		if(ItemController.TotalItems() == totalItemCount)
		{
			searchItemControllers = ItemController.ChooseSearchItems(MAX_ITEMS_TO_FIND);
			updateDelegate = null;
			onGuiUpdateDelegate = OnGuiDrawSearchList;
		}
	}
}
