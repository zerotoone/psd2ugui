using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//------------------------------------------------------------------------------
// class definition
//------------------------------------------------------------------------------
public class ItemController : MonoBehaviour
{
	static private List<ItemController> itemControllers;
	
	[SerializeField]
	private bool[] hitTestData;
	[SerializeField]
	private int hitTestDataStride;
	[SerializeField]
	private GameObject[] imageGos;
	[SerializeField]
	private HogScene.LayerType layerType;
	[SerializeField]
	private Rect worldSpaceRect;
	
	//----------------------------------------------------------------------
	// static public methods
	//----------------------------------------------------------------------
	static public ItemController[] ChooseSearchItems(int maxItems)
	{
		// first make a copy of the list containing only items
		List<ItemController> searchItemControllers = new List<ItemController>();
		foreach(ItemController ic in itemControllers)
		{
			if(ic.layerType == HogScene.LayerType.Item)
			{
				// randomly insert the item so it's shuffled
				searchItemControllers.Insert(Random.Range((int)0, searchItemControllers.Count), ic);
			}
		}
		// now simply clip the list to the max items, disabling the real item list by changing type
		int maxIndex = searchItemControllers.Count - 1;
		for(; maxIndex >= maxItems; maxIndex--)
		{
			searchItemControllers[maxIndex].layerType = HogScene.LayerType.Scenery;
		}
		if(maxItems > maxIndex + 1)
		{
			maxItems = maxIndex + 1;
		}
		searchItemControllers.RemoveRange(maxItems, searchItemControllers.Count - maxItems);
		return searchItemControllers.ToArray();
	}
	
	static public int TotalItems()
	{
		if(itemControllers != null)
		{
			return itemControllers.Count;
		}
		return 0;
	}
	
	static public ItemController PickItem(Vector3 position)
	{
		if(itemControllers == null)
		{
			return null;
		}
		
		// loop through list and do hit testing
		for(int i = 0; i < itemControllers.Count; i++)
		{
			// only process enabled objects
			if(itemControllers[i].gameObject.activeSelf == false)
			{
				continue;
			}
			
			// early out rectangle test first
			if(itemControllers[i].worldSpaceRect.Contains(position) == false)
			{
				continue;
			}
			
			// that passed, now do a hit test
			int y = (int)(position.y - itemControllers[i].worldSpaceRect.y);
			if(y >= itemControllers[i].hitTestData.Length/itemControllers[i].hitTestDataStride)
			{
				Debug.Log (y + " exceeds array length of " + (itemControllers[i].hitTestData.Length/itemControllers[i].hitTestDataStride) + " for object " + itemControllers[i].name);
				y = (itemControllers[i].hitTestData.Length/itemControllers[i].hitTestDataStride) - 1;
			}
			int x = (int)(position.x - itemControllers[i].worldSpaceRect.x);
			if(x >= itemControllers[i].hitTestDataStride)
			{
				Debug.Log (x + " exceeds array length of " + itemControllers[i].hitTestDataStride + " for object " + itemControllers[i].name);
				x = itemControllers[i].hitTestDataStride - 1;
			}
			if(itemControllers[i].hitTestData[(y * itemControllers[i].hitTestDataStride) + x] == false)
			{
				continue;
			}
			return itemControllers[i];
		}
		
		// can get here when in edit mode because the camera update is still being called
		// and cursor outside the window
		return null;
	}
	
	static public void WriteItemListToConsole()
	{
		for(int i = 0; i < itemControllers.Count; i++)
		{
			Debug.Log (itemControllers[i].name + " has depth " + itemControllers[i].transform.position.z);
		}
	}
	
	//----------------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------------
	public void AddHitTestData(bool[] newHitTestData, int newHitTestDataStride, Rect newWorldSpaceRect)
	{
		hitTestData = newHitTestData;
		hitTestDataStride = newHitTestDataStride;
		worldSpaceRect = newWorldSpaceRect;
	}
	
	public void AddImage(HogScene.ImageType imageType, GameObject imageGo)
	{
		if(imageGos == null)
		{
			imageGos = new GameObject[(int)HogScene.ImageType.Count];
		}
		imageGos[(int)imageType] = imageGo;

		// if we have an obscured image, hide the whole
		if((imageGos[(int)HogScene.ImageType.Obscured] != null) && (imageGos[(int)HogScene.ImageType.Whole] != null))
		{
			imageGos[(int)HogScene.ImageType.Whole].SetActive(false);
		}
	}
	
	public void AddLayer(HogScene.LayerType layerType)
	{
		this.layerType = layerType;
	}
	
	public HogScene.ImageType BestHitTestImageType()
	{
		// the images are stored in order of hit test preference, so return first one
		for(int i = 0; i < imageGos.Length; i++)
		{
			if(imageGos[i] != null)
			{
				return (HogScene.ImageType)i;
			}
		}
		
		// should never get here
		Debug.Log ("No images found for item " + gameObject.name);
		return 0;
	}
	
	public bool IsItem()
	{
		return (layerType == HogScene.LayerType.Item);
	}
	
	public void PickUpItem()
	{
		gameObject.SetActive(false);
	}
	
	//----------------------------------------------------------------------
	// protected mono methods
	//----------------------------------------------------------------------
	protected void Awake()
	{
		// does the pool exist yet?
		if(itemControllers == null)
		{
			// lazy initialize it
			itemControllers = new List<ItemController>();
		}
	}
	
	protected void OnDestroy()
	{
		if(itemControllers != null)
		{
			// remove myself from the pool
			itemControllers.Remove(this);
			// was I the last one?
			if(itemControllers.Count == 0)
			{
				// remove the pool itself
				itemControllers = null;
			}
		}
	}
	
	protected void OnDisable()
	{
	}
	
	protected void OnEnable()
	{
	}
	
	protected void Start()
	{
		// add myself, but sorted in the right place
		int insertionIndex = 0;
		for(; insertionIndex < itemControllers.Count; insertionIndex++)
		{
			if(transform.position.z < itemControllers[insertionIndex].transform.position.z)
			{
				break;
			}
		}
		itemControllers.Insert(insertionIndex, this);
	}
	
	protected void Update()
	{
	}

	//----------------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------------
}
