﻿#target estoolkit
var win, windowResource;

artLayerResource = "dialog {  \
    orientation: 'column', \
    alignChildren: ['fill', 'top'],  \
    preferredSize:[500, 330], \
    text: 'ScriptUI Window - dialog',  \
    margins:15, \
    \
    typeGroup: Group{ \
        typeButton: Button { text: 'type', properties:{name:'type'}, size: [120,24], alignment:['left', 'center'] }, \
        typeList:DropDownList{properties:{size: [400,100], items:['image', 'text', 'btn', 'rect', 'skip']}}\
        } \
    \
    codeGroup: Group{ \
        codeButton: Button { text: 'code', properties:{name:'code'}, size: [120,24], alignment:['left', 'center'] }, \
        codetext: EditText { text: 'code', characters: 50, justify: 'left'} \
        } \
    \
    resGroup: Group{ \
        resButton: Button { text: 'res', properties:{name:'res'}, size: [120,24], alignment:['left', 'center'] }, \
        restext: EditText { text: '', characters: 50, justify: 'left'} \
        } \
    \
    s9: Checkbox { text:'是不是九宫格图', value: false }, \
    \
    parentGroup: Group{ \
        parentButton: Button { text: 'parent', properties:{name:'parent'}, size: [120,24], alignment:['left', 'center'] }, \
        parenttext: EditText { text: '', characters: 50, justify: 'left'} \
        } \
    \
    bottomGroup: Group{ \
        cancelButton: Button { text: 'Cancel', properties:{name:'cancel'}, size: [120,24], alignment:['left', 'center'] }, \
        applyButton: Button { text: 'Apply', properties:{name:'ok'}, size: [120,24], alignment:['left', 'center'] }, \
    }\
}"


layerSetsResource = "dialog {  \
    orientation: 'column', \
    alignChildren: ['fill', 'top'],  \
    preferredSize:[500, 330], \
    text: 'ScriptUI Window - dialog',  \
    margins:15, \
    parentGroup: Group{ \
        parentButton: Button { text: 'parent', properties:{name:'parent'}, size: [120,24], alignment:['left', 'center'] }, \
        parenttext: EditText { text: '', characters: 50, justify: 'left'} \
        } \
    \
    bottomGroup: Group{ \
        cancelButton: Button { text: 'Cancel', properties:{name:'cancel'}, size: [120,24], alignment:['left', 'center'] }, \
        applyButton: Button { text: 'Apply', properties:{name:'ok'}, size: [120,24], alignment:['left', 'center'] }, \
    }\
}"


main();

function main()
{
    var doc = app.activeDocument;
    var layer = doc.activeLayer;
    var name = layer.name;
    //alert(layer.name);

    var win = null;
    if(layer.typename == "LayerSet")
    {
        win = new Window(layerSetsResource);
    }
    else
    {
        win = new Window(artLayerResource);
    }

    var strs = name.split(";");
    if(layer.typename == "LayerSet")
    {
        var parent = strs[0].split("=");
        if(parent[0] == "parent")
            win.parentGroup.parenttext.text = parent[1];
    }
    else
    {
        if(layer.kind == LayerKind.TEXT)
        {
            win.typeGroup.typeList.selection = 1;
        }

        for(var i = 0; i < strs.length; ++i)
        {
            var names = strs[i].split("=");

            if(names[0] == "type")
            {
                if(names[1] == "image")
                    win.typeGroup.typeList.selection = 0;
                else if(names[1] == "text")
                    win.typeGroup.typeList.selection = 1;
                else if(names[1] == "btn")
                    win.typeGroup.typeList.selection = 2;
                else if(names[1] == "rect")
                    win.typeGroup.typeList.selection = 3;
                else if(names[1] == "skip")
                    win.typeGroup.typeList.selection = 4;
                else
                    win.typeGroup.typeList.selection = 0;
            }
            else if(names[0] == "code")
            {
                win.codeGroup.codetext.text = names[1];
            }
            else if(names[0] == "res")
            {
                win.resGroup.restext.text = names[1];
            }
            else if(names[0] == "s9")
            {
                win.s9.value = true;
            }
            else if(names[0] == "parent")
            {
                win.parentGroup.parenttext.text = names[1];
            }
        }
    }

    win.bottomGroup.applyButton.onClick = function() {
        var rename = "";
        if(layer.typename == "LayerSet")
        {
            rename += "parent="
            rename += win.parentGroup.parenttext.text + ";";
            layer.name = rename;

            var answer = confirm("将\n" + name + "\n重命名为\n" + rename + "\n成功");
            if(answer)
            {
                return win.close();
            }
        }
        else
        {
            if(win.typeGroup.typeList.selection.text == "skip")
            {
                rename += "type=";
                rename += win.typeGroup.typeList.selection.text + ";";
                layer.name = rename;
                var answer = confirm("将\n" + name + "\n重命名为\n" + rename + "\n成功");
                if(answer)
                {
                    return win.close();
                }
                else
                {
                    return;
                 }
            }
            
            // code
            if(win.codeGroup.codetext != "")
            {
                rename += "code=";
                rename += win.codeGroup.codetext.text + ";";
            }
            else
            {
                alert("没有对 code 进行设置");
                return;
            }
            
            // type
            if(layer.kind == LayerKind.TEXT)
            {
                if(win.typeGroup.typeList.selection.text != "text")
                {
                    alert("这是一个文本图层，但是 type 没有设置为 text");
                    return;
                }
            }
            else
            {
                rename += "type=";
                rename += win.typeGroup.typeList.selection.text + ";";

                if(win.typeGroup.typeList.selection.text == "image")
                {
                    if(win.resGroup.restext.text == "")
                    {
                        alert("当 type 为 image 的时候，必须这是 res ");
                        return;
                    }
                }

                if(win.resGroup.restext.text != "")
                {
                    rename += "res=";
                    rename += win.resGroup.restext.text + ";";
                }

                if(win.s9.value == true)
                {
                    rename += "s9=";
                    rename += "1;";
                }
                
                if(win.parentGroup.parenttext.text != "")
                {
                    rename += "parent=";
                    rename += win.parentGroup.parenttext.text + ";";
                }
            }
            
            layer.name = rename;
            var answer = confirm("将\n" + name + "\n重命名为\n" + rename + "\n成功");
            if(answer)
            {
                return win.close();
            }
        }
    };

    win.bottomGroup.cancelButton.onClick = function() {
      return win.close();
    };

    win.show();
}
