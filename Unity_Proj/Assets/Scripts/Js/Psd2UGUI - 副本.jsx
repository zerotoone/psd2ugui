﻿// **************************************************
// This file created by Brett Bibby (c) 2010-2013
// You may freely use and modify this file as you see fit
// You may not sell it
//**************************************************
// hidden object game exporter
//$.writeln("=== Starting Debugging Session ===");

// enable double clicking from the Macintosh Finder or the Windows Explorer
#target photoshop

// debug level: 0-2 (0:disable, 1:break on error, 2:break at beginning)
// $.level = 0;
// debugger; // launch debugger on next line

// setup global variables
var itemData = "";
var fileData = "";
var sceneData;
var sourcePsd;
var duppedPsd;
var destinationFolder;
var objectId = 0;
var format = 0;
var width = 0;
var height = 0;

// run the exporter
main();

// main entry point
function main()
{
	// got a valid document?
	if( app.documents.length <= 0 )
	{
		if(app.playbackDisplayDialogs != DialogModes.NO)
		{
			alert("You must have a document open to export!");
		}
        
		// quit, returning 'cancel' makes the actions palette not record our script
    	return 'cancel';
    }

	// ask for where the exported files should go
	destinationFolder = Folder.selectDialog("Choose the destination for export.");
	if(!destinationFolder)
	{
		return;
	}

	// cache useful variables
	sourcePsdName = app.activeDocument.name; 
	var layerCount = app.documents[sourcePsdName].layers.length;
	var layerSetsCount = app.documents[sourcePsdName].layerSets.length;

	if((layerCount <= 1)&&(layerSetsCount <= 0))
	{
		if(app.playbackDisplayDialogs != DialogModes.NO)
		{
			alert("You need a document with multiple layers to export!");
			// quit, returning 'cancel' makes the actions palette not record our script
			return 'cancel';
		}
	}

	// setup the units in case it isn't pixels
	var savedRulerUnits = app.preferences.rulerUnits;
	var savedTypeUnits = app.preferences.typeUnits;
	app.preferences.rulerUnits = Units.PIXELS;
	app.preferences.typeUnits = TypeUnits.PIXELS;

	// duplicate document so we can extract everythng we need
	duppedPsd = app.activeDocument.duplicate();
	duppedPsd.activeLayer = duppedPsd.layers[duppedPsd.layers.length-1];
	// clean it up
	removeAllTopLevelArtLayers(duppedPsd);
	//removeAllInvisibleArtLayers(duppedPsd);
	//removeAllEmptyLayerSets(duppedPsd);
	hideAllArtLayers(duppedPsd);
    var val = duppedPsd.layers[0];
	exportPsdInfo(val);
	duppedPsd.close(SaveOptions.DONOTSAVECHANGES);	
	app.preferences.rulerUnits = savedRulerUnits;
	app.preferences.typeUnits = savedTypeUnits;

	alert("导出 psd 信息 完成");
}

function exportPsdInfo(obj)
{
	++format;
	var index = 0;
	for(var i = obj.layers.length - 1; i >= 0 ; --i)
	{
		var layer = obj.layers[i];

		fileData += "info : " + layer.name + "  " + layer.typename + "  \n"; 
		if(layer.typename == "LayerSet")
		{
			exportLayerSets(layer, index);
		}
		else
		{
			sceneData +=getFormat() + "<Layer ";
			exportArtLayer(layer, index, false);
			sceneData +="/>\n";	
		}

		++index;
	}
}

// 返回占位符
function getFormat()
{
	var str = "";
	for(var i = 0; i < format; ++i)
	{
		str += "\t";
	}

	return str;
}

// 递归,由于 ps 的层级关系是，上面的图层可以盖住下面的图层，
// 而 unity 是下面的可以盖住上面的，
// 所以在解析 ps 文件信息时，需要从数组的最后一个到第一个
function exportLayerSets(obj, index)
{
	// 递归,由于 ps 的层级关系是，上面的图层可以盖住下面的图层，
	// 而 unity 是下面的可以盖住上面的，
	// 所以在解析 ps 文件信息时，需要从数组的最后一个到第一个

	// only use layersets
	// 当不是为组的时候，return
	if(obj.typename != "LayerSet")
	{
		return;
	}

    exportPsdInfo (obj);    
}

function exportArtLayer(obj, index, isparent)
{
    saveScenePng(duppedPsd, "", obj.name);
}

function saveScenePng(psd, type, fileName)
{
	// we should now have a single art layer if all went well
	psd.mergeVisibleLayers();
	// figure out where the top-left corner is so it can be exported into the scene file for placement in game
	// capture current size
	var height = psd.height.value;
	var width = psd.width.value;
	var top = psd.height.value;
	var left = psd.width.value;
	// trim off the top and left
	psd.trim(TrimType.TRANSPARENT, true, true, false, false);
	// the difference between original and trimmed is the amount of offset
	top -= psd.height.value;
	left -= psd.width.value;
	// trim the right and bottom
	psd.trim(TrimType.TRANSPARENT);
	// find center
	top += (psd.height.value / 2)
	left += (psd.width.value / 2)
	// unity needs center of image, not top left
	top = -(top - (height/2));
	left -= (width/2);

	// save the image
	var pngFile = new File(destinationFolder + "/" + fileName + ".png");
	var pngSaveOptions = new PNGSaveOptions();
	psd.saveAs(pngFile, pngSaveOptions, true, Extension.LOWERCASE);
	psd.close(SaveOptions.DONOTSAVECHANGES);
}		

// 隐藏所有图层
// 最终要用一个方法叫合并可见图层, 所以这个方法很有必要
function hideAllArtLayers(obj)
{
	for(var i = 0; i < obj.artLayers.length; i++)
	{
		obj.artLayers[i].allLocked = false;
		obj.artLayers[i].visible = false;
	}

	for( var i = 0; i < obj.layerSets.length; i++)
	{
		hideAllArtLayers(obj.layerSets[i]);
	}
}

// 移除所有不可见的层
function removeAllInvisibleArtLayers(obj)
{
	for(var i = obj.artLayers.length-1; 0 <= i; i--)
	{
		if(!obj.artLayers[i].visible)
		{
			obj.artLayers[i].remove();
		}
	} 

	for(var i = obj.layerSets.length-1; 0 <= i; i--)
	{
		removeAllInvisibleArtLayers(obj.layerSets[i]);
	}
}

// artlayer remove 
// Deletes the object 
function removeAllTopLevelArtLayers(obj)
{
	for(var i = obj.artLayers.length-1; 0 <= i; i--)
	{
		obj.artLayers[i].remove();
	} 
}

// 移除所有空的文件夹
function removeAllEmptyLayerSets(obj)
{
	var foundEmpty = true;
	for(var i = obj.layerSets.length-1; 0 <= i; i--)
	{
		if(removeAllEmptyLayerSets(obj.layerSets[i]))
		{
			obj.layerSets[i].remove();
		}
		else
		{
			foundEmpty = false;
		}
	}
	if(obj.artLayers.length > 0)
	{
		foundEmpty = false;
	}
	return foundEmpty;
}

// 将解析好的数据写入到本地
function writePsdData()
{
	// create export
	var strs = sourcePsdName.split(".");
	var sceneFile = new File(destinationFolder + "/" + strs[0] + "_info.xml");
	sceneFile.open('w');
	sceneFile.writeln(sceneData);
	sceneFile.close();

	sceneFile = new File(destinationFolder + "/" + strs[0] + "_debug.txt");
	sceneFile.open('w');
	sceneFile.writeln(fileData);
	sceneFile.close();

	//var strToHex = stringToHex(sceneData);
	//var hexToStr = stringToHex(strToHex);

	//sceneFile = new File(destinationFolder + "/strToHex.txt");
	//sceneFile.open('w');
	//sceneFile.writeln(strToHex);
	//sceneFile.close();

	//sceneFile = new File(destinationFolder + "/hexToStr.txt");
	//sceneFile.open('w');
	//sceneFile.writeln(hexToStr);
	//sceneFile.close();
}

// prepare the psd for export
function preparePsdForExport(psd)
{
	// create file
	debugFile = new File(destinationFolder + "/debug.txt");
	debugFile.open('w');
	
	// loop through file and dump what we find
	for(var layerSetIndex = 0; layerSetIndex < psd.layerSets.length - 1; layerSetIndex++)
	{
		var layerSet = layerSetIndex[layerSetIndex];
		for(var layerIndex = 0; layerIndex < layerSet.length; layerIndex++)
		{
			var layer = layerSet[layerIndex];
			debugFile.writeln(layer.name);
		}
	}
	
	// finish scene
	debugFile.close();
}

// 
function stringToHex(str)
{
	var val="";
	var ret = "";
	for(var i = 0; i < str.length; ++i)
	{
		//if（val == ret)
			//val = str.charCodeAt(i).toString(16);
		//else
			val += "," + str.charCodeAt(i).toString(16);
	}
	return val;
}

function hexToString(str)
{
	var val = "";
	var arr = str.split(",");
	for(var i = 0; i < arr.length;i++)
	{
		val += arr[i].fromCharCode(i);
	}
	return val;
}