﻿// **************************************************
// This file created by Brett Bibby (c) 2010-2013
// You may freely use and modify this file as you see fit
// You may not sell it
//**************************************************
// hidden object game exporter
//$.writeln("=== Starting Debugging Session ===");

// enable double clicking from the Macintosh Finder or the Windows Explorer
#target photoshop

// debug level: 0-2 (0:disable, 1:break on error, 2:break at beginning)
// $.level = 0;
// debugger; // launch debugger on next line

// setup global variables
var itemData = "";
var fileData = "";
var sceneData;
var sourcePsd;
var duppedPsd;
var destinationFolder;
var objectId = 0;
var format = 0;
var savedRulerUnits;
var savedTypeUnits;
var isRepeat = false;

// run the exporter
main();

// main entry point
function main()
{
	// got a valid document?
	if( app.documents.length <= 0 )
	{
		if(app.playbackDisplayDialogs != DialogModes.NO)
		{
			alert("You must have a document open to export!");
		}
        
		// quit, returning 'cancel' makes the actions palette not record our script
    	return 'cancel';
    }

	// cache useful variables
	sourcePsdName = app.activeDocument.name; 
	var layerCount = app.documents[sourcePsdName].layers.length;
	var layerSetsCount = app.documents[sourcePsdName].layerSets.length;

	if((layerCount <= 1)&&(layerSetsCount <= 0))
	{
		if(app.playbackDisplayDialogs != DialogModes.NO)
		{
			alert("You need a document with multiple layers to export!");
			// quit, returning 'cancel' makes the actions palette not record our script
			return 'cancel';
		}
	}

	// setup the units in case it isn't pixels
	savedRulerUnits = app.preferences.rulerUnits;
	savedTypeUnits = app.preferences.typeUnits;
	app.preferences.rulerUnits = Units.PIXELS;
	app.preferences.typeUnits = TypeUnits.PIXELS;

	// duplicate document so we can extract everythng we need
	duppedPsd = app.activeDocument.duplicate();
	duppedPsd.activeLayer = duppedPsd.layers[duppedPsd.layers.length-1];
	// clean it up
	removeAllTopLevelArtLayers(duppedPsd);
	//removeAllInvisibleArtLayers(duppedPsd);
	//removeAllEmptyLayerSets(duppedPsd);
	
	// 这里取第一个组，所有的有效图层都必须在这个组下面
	var val = duppedPsd.layers[0];
	exportPsdInfo(val);

	if(isRepeat == false)
	{
		var answer = confirm("命名格式没有问题");
	}
	else
	{
		alert("请修正重复命名");
	}


	duppedPsd.close(SaveOptions.DONOTSAVECHANGES);	
	app.preferences.rulerUnits = savedRulerUnits;
	app.preferences.typeUnits = savedTypeUnits;
}

function exportPsdInfo(obj)
{
	// 检查所有子节点有没有重名
	if(obj.typename == "LayerSet")
	{
		var ret = false;
		for(var j = 0; j < obj.artLayers.length - 1; ++j)
		{
			var one = obj.artLayers[j];
			var onecode = getObjCodeName(one);

			for(var k = j + 1; k < obj.artLayers.length; ++k)
			{
				var two = obj.artLayers[k];
				var twocode= getObjCodeName(two);

				if(twocode == onecode)
				{
					alert("\n" + getObjParentsName(one) + "\n和\n" + getObjParentsName(two) + "\n重复命名");
					ret = true;
					isRepeat = true;
					break;
				}
			}

			if(ret)
			{
				break;
			}
		}
	}

	var index = 0;
	for(var i = obj.layers.length - 1; i >= 0 ; --i)
	{
		var layer = obj.layers[i];
		if(layer.typename == "LayerSet")
		{
			exportLayerSets(layer, index);
		}
		else
		{
			exportArtLayer(layer, index, false);
		}
	}
}

function exportLayerSets(obj, index)
{
	if(obj.typename != "LayerSet")
	{
		return;
	}

	


	var parent = obj.parent;
	if(parent != null)
	{
		var ret = false;
		for(var i = 0; i < parent.artLayers.length; ++i)
		{
			if(parent.artLayers[i].name.toString().search(obj.name) != -1)
			{
				ret = true;
				exportArtLayer(parent.artLayers[i], index, true);
				exportPsdInfo(obj);	
				return;
			}
		}
		if(ret == false)
		{
			var str = getObjParentsName(obj);
			var answer = confirm(str + "   该组没有对应的父节点");
			close();
		}

		fileData += "\n" + obj.name +　"  没有关联图层";
	}
	else
	{
		alert(obj.name + "父节点为空");
	}
}

function exportArtLayer(obj, index, isparent)
{
	// 跳过
	if(obj.name.search("skip") != -1)
	{
		return;
	}

	// 如果是关联图层
	if(isparent == false)
	{
		var name = obj.name.toString();
		if(name.search("parent") != -1)
		{
			return;
		}
	}

	var name = obj.name.toString();

	// text
	if(obj.kind == LayerKind.TEXT)
	{
		
	}
	else
	{
		if(name.search("type") == -1)
		{
			var str = getObjParentsName(obj);
			var answer = confirm(str + "   该图层没有设置 type 类型");
			close();
		}

		if(name.search("rect") == -1)
		{
			if(name.search("res") == -1)
			{
				var str = getObjParentsName(obj);
				var answer = confirm(str + "  该图层没有设置 res 资源");
				close();
			}
		}
	}

	if(name.search("code") == -1)
	{
		var str = getObjParentsName(obj);
		var answer = confirm(str + "  该图层没有设置 code 名称");
		close();
	}
}

function getObjParentsName(obj)
{
	var str = obj.name.toString();
	var temp = obj;
	while(true)
	{
		if(temp.parent != null)
		{
			temp = temp.parent;
			str = temp.name + "/" + str;
		}
		else
		{
			break;
		}
	}

	return str;
}

function getObjCodeName(obj)
{
	var name = obj.name;
	var strs = name.split(";");
	for(var i = 0; i < strs.length; ++i)
    {
    	var names = strs[i].split("=");
    	if(names[0] == "code")
    	{
    		return names[1];
    	}
    }
}

function close()
{
	duppedPsd.close(SaveOptions.DONOTSAVECHANGES);	
	app.preferences.rulerUnits = savedRulerUnits;
	app.preferences.typeUnits = savedTypeUnits;
}

// 移除所有不可见的层
function removeAllInvisibleArtLayers(obj)
{
	for(var i = obj.artLayers.length-1; 0 <= i; i--)
	{
		if(!obj.artLayers[i].visible)
		{
			obj.artLayers[i].remove();
		}
	} 

	for(var i = obj.layerSets.length-1; 0 <= i; i--)
	{
		removeAllInvisibleArtLayers(obj.layerSets[i]);
	}
}

// artlayer remove 
// Deletes the object 
function removeAllTopLevelArtLayers(obj)
{
	for(var i = obj.artLayers.length-1; 0 <= i; i--)
	{
		obj.artLayers[i].remove();
	} 
}

// 移除所有空的文件夹
function removeAllEmptyLayerSets(obj)
{
	var foundEmpty = true;
	for(var i = obj.layerSets.length-1; 0 <= i; i--)
	{
		if(removeAllEmptyLayerSets(obj.layerSets[i]))
		{
			obj.layerSets[i].remove();
		}
		else
		{
			foundEmpty = false;
		}
	}
	if(obj.artLayers.length > 0)
	{
		foundEmpty = false;
	}
	return foundEmpty;
}
