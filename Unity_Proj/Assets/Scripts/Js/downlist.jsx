﻿#target estoolkit

var w, layout;    
layout = "dialog {  \
    dropdown: DropDownList { \
        size: [100,20] \
        properties: { \
            items: ['one','two','three'] \
        } \
    } \
}";

w = new Window(layout);  
w.dropdown.selection=1;  
w.dropdown.onChange= function(){  
    alert(w.dropdown.selection.text);  
}  
w.show();   