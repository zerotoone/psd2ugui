﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HandleImage : EditorWindow
{
	//TextureImporterType textureType = TextureImporterType.Sprite;

	[MenuItem("GameTools/HandleImage")]
	public static void Init()
	{
		HandleImage win = (HandleImage)EditorWindow.GetWindow(typeof(HandleImage), true, "HandleImage");
		win.ShowNotification(new GUIContent("替换操作无法撤销，请谨慎操作"));
	}

	void OnGUI()
	{
		if (GUILayout.Button("HandleImage"))
		{
			Object[] objs = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
			foreach (Object o in objs)
			{
				string path = AssetDatabase.GetAssetPath(o);
				if (path.Contains(".png"))
				{
					if (o is Texture2D)
					{
						Texture2D tex = o as Texture2D;
						SetTextureImporterFormat(tex, true);
						Debug.Log(path + "  Texture   " + tex.width + "  " + tex.height);
					}
				}
			}
		}
	}

	public static void SetTextureImporterFormat(Texture2D texture, bool isReadable)
	{
		if (null == texture) return;

		int maxSize = texture.width;
		if (texture.height > maxSize)
		{
			maxSize = texture.height;
		}

		string assetPath = AssetDatabase.GetAssetPath(texture);
		TextureImporter importer = AssetImporter.GetAtPath(assetPath) as TextureImporter;
		if (importer != null)
		{
			//importer.textureType = TextureImporterType.Advanced;
			//importer.isReadable = isReadable;
			importer.textureType = TextureImporterType.Sprite;
			importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;

			if (maxSize < 32)
			{
				importer.maxTextureSize = 32;
			}
			else if (maxSize < 64)
			{
				importer.maxTextureSize = 64;
			}
			else if (maxSize < 128)
			{
				importer.maxTextureSize = 128;
			}
			else if (maxSize < 246)
			{
				importer.maxTextureSize = 256;
			}
			else if (maxSize < 512)
			{
				importer.maxTextureSize = 512;
			}
			else if (maxSize < 1024)
			{
				importer.maxTextureSize = 1024;
			}
			else if (maxSize < 2048)
			{
				importer.maxTextureSize = 2048;
			}
			else
			{
				importer.maxTextureSize = 2048;
			}

			AssetDatabase.ImportAsset(assetPath);
			AssetDatabase.Refresh();
		}
	}
}
