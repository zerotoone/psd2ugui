﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine;
using System.Xml;

namespace Psd2UGUI
{
    public class UIImporter : EditorWindow
    {
        public GameObject root;
        public List<GameObject> parentList = new List<GameObject>();
        public int resolutionX = 2208;
        public int resolutionY = 1242;

        static UIImporter win = null;
        [MenuItem("GameTools/UIImporter")]
        static public void ImportUISceneMenuItem()
        {
            win = (UIImporter)EditorWindow.GetWindow(typeof(UIImporter), true, "UIImporter");
        }

        void OnGUI()
        {
            root = Selection.activeGameObject;
            EditorGUILayout.BeginVertical();
            EditorGUILayout.ObjectField("请选择根节点", root, typeof(GameObject), true, GUILayout.Height(35));

            if (GUILayout.Button("Importer UIScene with xml", GUILayout.Height(35)))
            {
                if (root == null)
                {
                    Debug.LogError("root is null");
                    win.Close();
                }
                parentList.Add(root);
                ImporterUIScene();
            }

            if (GUILayout.Button("Update UIScene with xml", GUILayout.Height(35)))
            {
                if (root == null)
                {
                    Debug.LogError("root is null");
                    win.Close();
                }

                parentList.Add(root);
                ImporterUIScene();
            }

            EditorGUILayout.EndVertical();
        }

        void ImporterUIScene()
        {
            // 选择 psd 导出的文件信息
            string inputFile = EditorUtility.OpenFilePanel("Choose photoshop file info  to Import", Application.dataPath + "/Art/Psd/", "xml");
            if ((inputFile != null) && (inputFile != "") && (inputFile.StartsWith(Application.dataPath)))
            {
                XmlDocument xml = LoadXml(inputFile);
                XmlNode scene = xml.SelectSingleNode("UIScene");
                XmlElement resolution = scene["Resolution"];
                resolutionX = Int32.Parse(resolution.GetAttribute("width"));
                resolutionY = Int32.Parse(resolution.GetAttribute("height"));

                XmlNodeList xmlNodeList = xml.SelectSingleNode("UIScene").ChildNodes;
                foreach (XmlElement element in xmlNodeList)
                {
                    LoadXmlNode(element);
                }
            }
        }

        // 加载 psd 文件信息的 xml
        XmlDocument LoadXml(string filePath)
        {
            Debug.Log("LoadXml path:" + filePath);

            XmlDocument xml = new XmlDocument();
            XmlReaderSettings set = new XmlReaderSettings();
            set.IgnoreComments = true;//这个设置是忽略xml注释文档的影响。有时候注释会影响到xml的读取
            xml.Load(XmlReader.Create(filePath, set));
            return xml;
        }

        void LoadXmlNode(XmlElement element)
        {
            if (element.GetAttribute("type").ToLower() == "image")
            {
                GenerateImage(element);
            }
            else if (element.GetAttribute("type").ToLower() == "text")
            {
                GenerateText(element);
            }
            else if (element.GetAttribute("type").ToLower() == "btn" || element.GetAttribute("type") == "button")
            {
                GenerateButton(element);
            }
            else if (element.GetAttribute("type").ToLower() == "rect")
            {
                GenerateRectPanel(element);
            }
            else if (element.GetAttribute("type").ToLower() == "scroll")
            {
                GenerateScroll(element);
            }
            else if (element.GetAttribute("type").ToLower() == "grid")
            {
                GenerateGrid(element);
            }
            else if (element.GetAttribute("type").ToLower() == "loop")
            {
                GenerateLoop(element);
            }
            else if (element.GetAttribute("type").ToLower() == "slider")
            {
                GenerateSlider(element);
            }
            else
            {
                Debug.Log(element.GetAttribute("type") + "TODO...");
            }
        }

        /*
        <Layer type="text" content="服务器" colorr="255" colorg="255" colorb="255" 
        font="STYuanti-SC-Regular" fontsize="40" code="server" opacity="255" 
        bounds1="1041" bounds2="863" bounds3="1168" bounds4="902" />
        */
        void GenerateText(XmlElement element)
        {
            GameObject obj = null;
            try
            {
                obj = parentList[parentList.Count - 1].transform.FindChild(element.GetAttribute("code")).gameObject;
            }
            catch
            {
                obj = Instantiate(Resources.Load("Prefabs/Text")) as GameObject;
            }
            Text text = obj.GetComponent<Text>();
            // content
            text.text = element.GetAttribute("content");
            // font todo...
            //txt.font = Int32.Parse(element.GetAttribute("font"));

            // font size
            text.fontSize = Int32.Parse(element.GetAttribute("fontsize"));

            // color todo...
            text.color = new Color(float.Parse(element.GetAttribute("colorr")) / 255.0f,
                float.Parse(element.GetAttribute("colorg")) / 255.0f,
                float.Parse(element.GetAttribute("colorb")) / 255.0f,
                float.Parse(element.GetAttribute("opacity")) / 255.0f);

            GenerateRectTransform(element, obj);
        }

        /*
        <Layer type="image" code="topbg" res="Ui_ziseanniu"  opacity="255" 
        bounds1="933" bounds2="836" bounds3="1277" bounds4="945" 
        s9="1"/>
        */
        // image 图片 
        void GenerateImage(XmlElement element)
        {
            GameObject obj = null;
            try
            {
                obj = parentList[parentList.Count - 1].transform.FindChild(element.GetAttribute("code")).gameObject;
            }
            catch
            {
                obj = Instantiate(Resources.Load("Prefabs/Image")) as GameObject;
            }

            Image image = obj.GetComponent<Image>();

            // res name
            string[] asset = AssetDatabase.FindAssets(element.GetAttribute("res"));
            //  TODO... 检测文件重命名
            //if(asset.Length > 2)
            //{
            //    Debug.LogError(element.GetAttribute("res") + "文件有重复的命名");
            //    foreach (var item in asset)
            //    {
            //        Debug.Log(AssetDatabase.GUIDToAssetPath(item));
            //    }
            //    return;
            //}

            string path = AssetDatabase.GUIDToAssetPath(asset[0]);
            image.sprite = (Sprite)AssetDatabase.LoadAssetAtPath(path, typeof(Sprite));
            image.SetNativeSize();

            // 九宫格
            if (element.HasAttribute("s9"))
            {
                if (element.GetAttribute("s9") == "1")
                {
                    image.type = Image.Type.Sliced;
                    image.preserveAspect = true;
                }
            }
            else
            {
                image.type = Image.Type.Simple;
            }

            // 不透明度
            if (element.HasAttribute("opacity"))
            {
                int opacity = Int32.Parse(element.GetAttribute("opacity"));
                image.color = new Color(image.color.r, image.color.g, image.color.b, opacity / 255);
            }

            GenerateRectTransform(element, obj);
        }

        // button  按钮
        void GenerateButton(XmlElement element)
        {
            GameObject obj = null;
            try
            {
                obj = parentList[parentList.Count - 1].transform.FindChild(element.GetAttribute("code")).gameObject;
            }
            catch
            {
                obj = Instantiate(Resources.Load("Prefabs/Button")) as GameObject;
            }
            Image image = obj.GetComponent<Image>();

            string[] asset = AssetDatabase.FindAssets(element.GetAttribute("res"));
            string path = AssetDatabase.GUIDToAssetPath(asset[0]);
            image.sprite = (Sprite)AssetDatabase.LoadAssetAtPath(path, typeof(Sprite));
            image.SetNativeSize();

            // 九宫格
            if (element.HasAttribute("s9"))
            {
                if (element.GetAttribute("s9") == "1")
                {
                    image.type = Image.Type.Sliced;
                    image.preserveAspect = true;
                }
            }
            else
            {
                image.type = Image.Type.Simple;
            }

            // 不透明度
            if (element.HasAttribute("opacity"))
            {
                int opacity = Int32.Parse(element.GetAttribute("opacity"));
                image.color = new Color(image.color.r, image.color.g, image.color.b, opacity / 255);
            }

            GenerateRectTransform(element, obj);
        }

        // 面板
        void GenerateRectPanel(XmlElement element)
        {
            GameObject obj = null;
            try
            {
                obj = parentList[parentList.Count - 1].transform.FindChild(element.GetAttribute("code")).gameObject;
            }
            catch
            {
                obj = Instantiate(Resources.Load("Prefabs/RectPanel")) as GameObject;
            }
            GenerateRectTransform(element, obj);
        }

        // scroll
        void GenerateScroll(XmlElement element)
        {
            GameObject obj = null;
            try
            {
                obj = parentList[parentList.Count - 1].transform.FindChild(element.GetAttribute("code")).gameObject;
            }
            catch
            {
                obj = Instantiate(Resources.Load("Prefabs/Scroll")) as GameObject;
            }
            GenerateRectTransform(element, obj);
        }

        // grid
        void GenerateGrid(XmlElement element)
        {
            GameObject obj = null;
            try
            {
                obj = parentList[parentList.Count - 1].transform.FindChild(element.GetAttribute("code")).gameObject;
            }
            catch
            {
                obj = Instantiate(Resources.Load("Prefabs/Grid")) as GameObject;
            }
            GenerateRectTransform(element, obj);
        }

        // loop
        void GenerateLoop(XmlElement element)
        {
            GameObject obj = null;
            try
            {
                obj = parentList[parentList.Count - 1].transform.FindChild(element.GetAttribute("code")).gameObject;
            }
            catch
            {
                obj = Instantiate(Resources.Load("Prefabs/Loop")) as GameObject;
            }
            GenerateRectTransform(element, obj);
        }

        // slider
        void GenerateSlider(XmlElement element)
        {
            GameObject obj = null;
            try
            {
                obj = parentList[parentList.Count - 1].transform.FindChild(element.GetAttribute("code")).gameObject;
            }
            catch
            {
                obj = Instantiate(Resources.Load("Prefabs/Slider")) as GameObject;
            }
            GenerateRectTransform(element, obj);
        }

        // 生成 object 的 Rect 属性
        void GenerateRectTransform(XmlElement element, GameObject obj)
        {
            obj.name = element.GetAttribute("code");
            obj.transform.SetParent(root.transform);
            obj.transform.SetSiblingIndex(Int32.Parse(element.GetAttribute("index")));
            obj.transform.localScale = new Vector3(1, 1, 1);

            //Vector2 anchormin = obj.transform.GetComponent<RectTransform>().anchorMin;
            //Vector2 anchormax = obj.transform.GetComponent<RectTransform>().anchorMax;
            //Vector2 pivot = obj.transform.GetComponent<RectTransform>().pivot;

            obj.transform.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
            obj.transform.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
            obj.transform.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

            //Debug.Log("code   " + element.GetAttribute("code") + "   " + obj.transform.GetComponent<RectTransform>().anchoredPosition.x + "   " + obj.transform.GetComponent<RectTransform>().anchoredPosition.y);

            // size 
            float width = float.Parse(element.GetAttribute("bounds3")) - float.Parse(element.GetAttribute("bounds1"));
            float height = float.Parse(element.GetAttribute("bounds4")) - float.Parse(element.GetAttribute("bounds2"));
            obj.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);

            // pos
            // posx = psx + image.width/2 - screen.width/2
            float posx = float.Parse(element.GetAttribute("bounds1")) + width / 2 - resolutionX / 2;
            // posy = -(psy+image.height/2-screen.height/2)
            float posy = -(float.Parse(element.GetAttribute("bounds2")) + height / 2 - resolutionY / 2);
            Debug.Log("code   " + element.GetAttribute("code") + "   " + posx + "   " + posy);

            obj.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(posx, posy);
            obj.transform.SetParent(parentList[parentList.Count - 1].transform);

            //obj.transform.GetComponent<RectTransform>().anchorMin = anchormin;
            //obj.transform.GetComponent<RectTransform>().anchorMax = anchormax;
            //obj.transform.GetComponent<RectTransform>().pivot = pivot;
            //Debug.Log("code   " + element.GetAttribute("code") + "   " + obj.transform.GetComponent<RectTransform>().anchoredPosition.x + "   " + obj.transform.GetComponent<RectTransform>().anchoredPosition.y);


            // 子节点
            parentList.Add(obj);
            foreach (XmlElement item in element.ChildNodes)
            {
                LoadXmlNode(item);
            }
            parentList.Remove(obj);
        }
    }
}
