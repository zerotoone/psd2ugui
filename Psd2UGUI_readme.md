## Psd2UGUI Readme

-----------------------
### 使用流程
- 打开 Photoshop
- 文件 -> 脚本 -> 浏览 -> 选择 Psd2UGUI.jsxbin 脚本 -> 载入
- 导出当前 psd 文件，选择导出路径，选择确定按钮。导出 psd 信息完成
- 将导出来的 psd 信息，拷贝到 VS 中， 按下 Ctrl + K，Ctrl + D ,格式化文件
- 在 unity 中，选择根节点，然后 GameTools -> UIImporter -> importer UIScene with xml, 选择对应的 xml 文件。
- 当已经生成过一次 prefab 时，psd 文件有更新，这时选择 update UIScene with xml, 然后选择对应的 xml 文件
- 完成

-------------------------
### 参数说明
- res(resname)： 引用资源的名字 (res=Ui_anniu_02)
- code(codename)： 程序中代码需要用到的名字 (code=startbtn)
- type： 标志当前图层的类型，目前支持 text,image,rect，btn(button)，slider,scrool,loop,grid （type=image）
- s9： 使用九宫格图片 (s9=1)
- opacity： 不透明度（透明度）（opacity=255）
- bounds1,bounds2,bounds3,bounds4： 当前 psd 文件中图层的 bounds，记录图层的大小和位置
- font： text 独占属性， text 的字体属性
- fontsize： text 独占属性， text 的字体大小
- colorr，colorg，colorb： text 独占属性， text 的字体颜色 
- skip： 跳过，程序将不会处理这个图层
- parent： 这个图层有子图层，子图层必须要一个组下面，组的命名必须要和这个图层后面 parent 属性一样，例如：当前图层为：parent=start;res=Ui_anniu_02;type=btn;code=startbtn;s9=1，子图层组为：parent=start。当前图层为：parent=change;res=Ui_anniu_01;type=btn;code=changebtn;s9=1，子图层组为：parent=change

-------------------------
### 技术实现难点
- 利用 JavaScript 解析 psd 文件信息，写入到 xml 中。由于 ps 的层级关系是上面的图层可以盖住下面的，而 unity 相反。所以在解析 psd 的时候，需要反向读取 psd 的图层信息。
- 由于 psd 的组是不能记录信息的，没有大小和位置，所以必须使用一个图层来记录信息。将他们使用相同的名字关联起来，用这个图层来作为这个组的所有图层的父节点。例如：当前图层为：parent=change;res=Ui_anniu_01;type=btn;code=changebtn;s9=1;子图层组为：parent=change;
- 解析图层时，递归读取所有图层,必须按照 psd 中文件的顺序，从下向上一个图层一个组的读取。当读取组的时候递归。
- 将 psd 文件中图层的信息转换为 unity 中 gameobject 的信息：width = bounds3 - bounds1; height = bounds4 - bounds2; posx = bounds1 + width / 2 - psd.width / 2; posy = -(bounds2 + height / 2 - psd.height / 2);
- 由于在 psd 中得到每一个图层的 bounds 都是相对于 psd 文件的左上角的信息，没有父节点组件坐标系概念。所以转换到 unity 中时候需特殊处理。<font color="red">必须要先把生成的节点的父节点设置为根节点。设置好位置和大小之后，在设置节点的真实父节点。这样 unity 就会自动进行坐标转换，而且节点的位置不会变化。(或许还有其他解决办法）</font>


------------------------
### 追加功能
- 添加各种控件支持
- unity UGUI 锚点转换比较复杂，现在仅支持中心的九个锚点。
- 完美


### bug 

